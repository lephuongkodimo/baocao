/*
 * main2.cpp
 *
 *  Created on: Dec 4, 2018
 *      Author: Admin
 */

#include "Arduino.h"


#include "nvs.h"
#include "nvs_flash.h"
#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEServer.h>
#define SERVICE_UUID                "6261CB01-F5EA-490E-8548-E6821E3E7792"
#define CHARACTERISTIC_UUID         "6261EC06-F5EA-490E-8548-E6821E3E7792"
#define UUID_BLOCK_ID 				"62614C14-F5EA-490E-8548-E6821E3E7792"
const char* UUID_BASE = "62610000-F5EA-490E-8548-E6821E3E7792";
class MyCallbacks: public BLECharacteristicCallbacks {
    void onWrite(BLECharacteristic *pCharacteristic) {
      std::string value = pCharacteristic->getValue();
      Serial.printf("%s\r\n",value.c_str());
    }
};
extern "C" {
void app_main(void);
}

void app_main(void) {
Serial.begin(115200);
esp_err_t err = nvs_flash_init();
if (err == ESP_ERR_NVS_NO_FREE_PAGES) {
    // OTA app partition table has a smaller NVS partition size than the non-OTA
    // partition table. This size mismatch may cause NVS initialization to fail.
    // If this happens, we erase NVS partition and initialize NVS again.
    ESP_ERROR_CHECK(nvs_flash_erase());
    err = nvs_flash_init();
}
Serial.println("Starting Arduino");
Serial.println("Starting Arduino BLE Client application...");
BLEDevice::init("BOBO");
BLEServer *pServer = BLEDevice::createServer();
BLEService *pService = pServer->createService(BLEUUID(SERVICE_UUID));
BLECharacteristic *pCharacteristic = pService->createCharacteristic(
                                       CHARACTERISTIC_UUID,
                                       BLECharacteristic::PROPERTY_READ |
                                       BLECharacteristic::PROPERTY_WRITE
                                     );
pCharacteristic->setCallbacks(new MyCallbacks);
//pCharacteristic->setValue("Hello World says Neil");
pService->start();
BLEAdvertising *pAdvertising = pServer->getAdvertising();
pAdvertising->addServiceUUID(UUID_BASE);
pAdvertising->start();
Serial.println("Characteristic defined! Now you can read it in your phone!");
while(1){

	delay(2000);
}
}
