#ifndef MAIN_MPU6050_H_
#define MAIN_MPU6050_H_

#include <math.h>
#include "I2C.h"
#include <driver/gpio.h>
#include "Arduino.h"
#include <algorithm>
#define I2C_ADDRESS_L 0x68 // I2C address of MPU6050
#define I2C_ADDRESS_H 0x69 // I2C address of MPU6050

#define MPU6050_ACCEL_XOUT_H 0x3B
#define MPU6050_GYRO_XOUT_H  0x43
#define MPU6050_PWR_MGMT_1   0x6B
#define INT_ENABLE 		     0x38
#define FIFO_EN				 0x23
#define MPU6050_GYRO_CONFIG  0x1b
#define SIGNAL_PATH_RESET  0x68
#define I2C_SLV0_ADDR      0x37
#define ACCEL_CONFIG       0x1C
#define MOT_THR            0x1F  // Motion detection threshold bits [7:0]
#define MOT_DUR            0x20  // Duration counter threshold for motion interrupt generation, 1 kHz rate, LSB = 1 ms
#define MOT_DETECT_CTRL    0x69
#define INT_ENABLE         0x38
#define WHO_AM_I_MPU6050   0x75 // Should return 0x68

#define RAD_TO_DEG (57.295778f)
/**
 * @brief Driver for the %MPU6050 accelerometer and gyroscope.
 *
 * The %MPU6050 uses %I2C as the communication between the master and the slave.  The class stores
 * the last read values as class instance local data.  The data can be retrieved from the class
 * using the appropriate getters.  The readAccel() and readGyro() methods cause communication
 * with the device to retrieve and locally hold the values from the device.
 *
 * A call to init() must precede all other API calls.
 */
class MPU6050 {
private:
	I2C *i2c;
	short accel_x, accel_y, accel_z;
	short gyro_x, gyro_y, gyro_z;
	double  roll  ,pitch ,yaw;
	bool inited;
	float gyroXoffset = 0.0f, gyroYoffset=0.0f, gyroZoffset=0.0f;
	float accXoffset, accYoffset, accZoffset;
	float  accX, accY, accZ, gyroX, gyroY, gyroZ;
public:
	MPU6050();
	virtual ~MPU6050();
	void calcGyroOffsets(bool console = false);
	float getAccZoffset();
//-----------------------------------------------------------//
	float getAccX(){ return accX; };
	float getAccY(){ return accY; };
	float getAccZ(){ return accZ; };
	float getGyroX(){ return gyroX; };
	float getGyroY(){ return gyroY; };
	float getGyroZ(){ return gyroZ; };
	/**
	 * @brief Get the X acceleration value.
	 */
	short getRawAccelX() const
	{
		return accel_x;
	}

	/**
	 * @brief Get the Y acceleration value.
	 */
	short getRawAccelY() const
	{
		return accel_y;
	}

	/**
	 * @brief Get the Z acceleration value.
	 */
	short getRawAccelZ() const
	{
		return accel_z;
	}
//-----------------------------------------------------//
	/**
		 * @brief Get the X acceleration value.
		 */
	double getPoll() const
		{
			return roll;
		}

		/**
		 * @brief Get the Y acceleration value.
		 */
	double getPitch() const
		{
			return pitch;
		}

		/**
		 * @brief Get the Z acceleration value.
		 */
	double getYaw() const
		{
			return yaw;
		}

//-----------------------------------------------------//
	/**
	 * @brief Get the X gyroscopic value.
	 */
	short getRawGyroX() const
	{
		return gyro_x;
	}

	/**
	 * @brief Get the Y gyroscopic value.
	 */
	short getRawGyroY() const
	{
		return gyro_y;
	}

	/**
	 * @brief Get the Z gyroscopic value.
	 */
	short getRawGyroZ() const
	{
		return gyro_z;
	}

	/**
	 * @brief Get the magnitude of the acceleration.
	 *
	 * Since acceleration is a vector quantity, it has both direction and magnitude.  This
	 * method returns the currently known magnitude of the last read data.
	 *
	 * @return The magnitude of the acceleration.
	 */
	uint32_t getMagnitude() {
		return sqrt(accel_x * accel_x + accel_y * accel_y + accel_z * accel_z);
	}

	void init(uint8_t address,gpio_num_t sdaPin=I2C::DEFAULT_SDA_PIN, gpio_num_t clkPin=I2C::DEFAULT_CLK_PIN,uint32_t clkSpeed=I2C::DEFAULT_CLK_SPEED,i2c_port_t portNum=I2C_NUM_0);
	void readAccel();
	void calRPY();
	void readGyro();
	 void writeByte( uint8_t subAddress, uint8_t data);
	/**
	 *dang ki interup cho mpu
	 *
	 */

	void setupInterup();
};

#endif /* MAIN_MPU6050_H_ */
