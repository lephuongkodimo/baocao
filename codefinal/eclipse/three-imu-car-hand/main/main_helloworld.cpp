/*
 * 1. Open up the project properties
 * 2. Visit C/C++ General > Preprocessor Include Paths, Macros, etc
 * 3. Select the Providers tab
 * 4. Check the box for "CDT GCC Built-in Compiler Settings"
 * 5. Set the compiler spec command to "xtensa-esp32-elf-gcc ${FLAGS} -E -P -v -dD "${INPUTS}""
 * 6. Rebuild the index
*/

#include <esp_log.h>
#include <string>
#include "sdkconfig.h"
#include "Arduino.h"
#include "BLEDevice.h"
#include "nvs.h"
#include "nvs_flash.h"
#include <stdio.h>
#include <cstdio>
#include <iostream>   // std::cout
#include  <MPU6050.h>
#include "driver/gpio.h"

//#include "mpu6050_tockn.h"
#include <MatrixMath.h>
#include "MadgwickAHRS.h"
#include "MahonyAHRS.h"
#include "quaternion.h"
//#include "GSBLEconnet.h"

#include "filter.h"
#include <sstream>
#include "Kalman.h"
#include "driver/gpio.h"
#include "esp_system.h"
#include <cmath>
//#include "BLEScan.h"
#define secondIMU
#define DRAW
#define threeIMU
#define GPIO_ON_IMU_3 26
#define IMU_THREE_SDA GPIO_NUM_18
#define IMU_THREE_SCL GPIO_NUM_19
#define LED_R GPIO_NUM_12
#define LED_Y GPIO_NUM_2
#define LED_G GPIO_NUM_14
#define BUTTON (GPIO_NUM_33)
#define BUTTON_MODE GPIO_NUM_0
#define I2C_SPEED 400000
#define UUID_BASE 			"62610000-F5EA-490E-8548-E6821E3E7792"

#define UUID_PROGRAM_SERVICE		"6261CB01-F5EA-490E-8548-E6821E3E7792"
#define UUID_BLOCK_ID 				"62614C14-F5EA-490E-8548-E6821E3E7792"
#define UUID_PROGRAM 				"6261EC06-F5EA-490E-8548-E6821E3E7792"

// The remote service we wish to connect to.
static BLEUUID serviceUUID(UUID_PROGRAM_SERVICE);
// The characteristic of the remote service we are interested in.
static BLEUUID    charUUID(UUID_PROGRAM);

static BLEAddress *pServerAddress;
static boolean doConnect = false;
static boolean connected = false;
static BLERemoteCharacteristic* pRemoteCharacteristic;
/*
 * *************************************************************************
 */
Madgwick filter_esp;
Madgwick filter_espH;
//Mahony filter_esp;
const uint8_t SCALE_GRAPH = 10;
const float Radius = 100.0;
static bool MODE_CAR_AND_DIGI = true;

#ifdef TESTING
#include "dataorgin.h"
#endif

#define N 256
const int DONE_RICIVE_DATA_BIT = BIT0;
const int PRESS_BIT = BIT1;
const int RICIVE_DATA_BIT = BIT2;
const int RICIVE_POINT_DATA_BIT = BIT3;
static EventGroupHandle_t imu_event_group;
unsigned long microsPrevious;
unsigned long microsPrevious_2;
const unsigned long microsPerReading = 3906;//3906; //100000/256

/*
 * *********************************************************************************
 */


const float samplePeriod[1] = { 0.00390625f };
const double samplePeriod_d[1] = { 0.004 };
const float Earth[3] = { 0.0f, 0.0f, 1.0f };
const float g[1] = { 9.81f };
int fc = 0;
static int count = 0;
static bool haveData = false;
unsigned long microsStart;
unsigned long microsEnd;
static int taskCore_s = 0;
static int taskCore_r = 1;
const float gyroScale = 0.0175f;  //covet deg/s to radian/s for mahony

float Alphat;
float rAlphat;
float tAlphat = 0.0;

static float gtx, gty, gtz;  //deg/s
static float atx, aty, atz;  //g
static float roll;
static float pitch;
static float yaw;

static float gtxH, gtyH, gtzH;  //deg/s
static float atxH, atyH, atzH;  //g
static float rollH;
static float pitchH;
static float yawH;

static float gtxL_2, gtyL_2, gtzL_2;  //deg/s
static float atxL_2, atyL_2, atzL_2;  //g
static float rollL_2;
static float pitchL_2;
static float yawL_2;
static float Distance = 0.0 ; // cai so voi org
static float TotalDistance = 0.0 ; // cai so voi org
static float Alpha = 0.0 ;
static float tAlpha = 0.0 ;
static float rDistance = 0.0 ; //cai nay la cai that su can cua = tinh dc - cai tinh duoc truoc do
static float rAlpha = 0.0 ;

static float x, y,z , x_0 =0.0 ,y_0 = 0.0, _x0= 0.0,_y0=0.0;
static uint16_t cPoint = 0;
float filteredArray[N][3];
unsigned long deltaTime;//unsigned long deltaTime;
unsigned long deltaTime_2;//unsigned long deltaTime;
static std::vector<String> vDistance {
//	String("200,0,-1;"),
};
static std::vector<String> vAlpha{
//	String("210,0,0"),
};
//*================================
static MPU6050 mpu6050L;
static MPU6050 mpu6050H;

static MPU6050 mpu6050L_2;
static char tag[]="cpp_helloworld";



extern "C" {
	void app_main(void);
}
void BLETask(void*param);
void led()
{
    /* Configure the IOMUX register for pad BLINK_GPIO (some pads are
       muxed to GPIO on reset already, but some default to other
       functions and need to be switched to GPIO. Consult the
       Technical Reference for a list of pads and their default
       functions.)
    */
    gpio_pad_select_gpio(LED_R);
    /* Set the GPIO as a push/pull output */
    gpio_set_direction(LED_R, GPIO_MODE_OUTPUT);
    gpio_pad_select_gpio(LED_Y);
    /* Set the GPIO as a push/pull output */
    gpio_set_direction(LED_Y, GPIO_MODE_OUTPUT);
    gpio_pad_select_gpio(LED_G);
    /* Set the GPIO as a push/pull output */
    gpio_set_direction(LED_G, GPIO_MODE_OUTPUT);
    gpio_pad_select_gpio(GPIO_NUM_25);
        /* Set the GPIO as a push/pull output */
        gpio_set_direction(GPIO_NUM_25, GPIO_MODE_OUTPUT);
    gpio_set_level(LED_G, 1);
    gpio_set_level(LED_Y, 1);
    gpio_set_level(LED_R, 1);
    gpio_set_level(GPIO_NUM_25, 1);
}
class MyCallbackConnect: public BLEClientCallbacks{
	virtual void onConnect(BLEClient *pClient){

		;
	}
	virtual void onDisconnect(BLEClient *pClient) {
		connected = false;
		Serial.println("Disconnect... to reconnect");
		;
	}
};
static void notifyCallback(
  BLERemoteCharacteristic* pBLERemoteCharacteristic,
  uint8_t* pData,
  size_t length,
  bool isNotify) {
    Serial.print("Notify callback for characteristic ");
    Serial.print(pBLERemoteCharacteristic->getUUID().toString().c_str());
    Serial.print(" of data length ");
    Serial.println(length);
}

bool connectToServer(BLEAddress pAddress) {
    Serial.print("Forming a connection to ");
    Serial.println(pAddress.toString().c_str());

    BLEClient*  pClient  = BLEDevice::createClient();
    pClient->setClientCallbacks(new MyCallbackConnect);
    Serial.println(" - Created client");

    // Connect to the remove BLE Server.
    pClient->connect(pAddress);
    Serial.println(" - Connected to server");

    // Obtain a reference to the service we are after in the remote BLE server.
    BLERemoteService* pRemoteService = pClient->getService(serviceUUID);
    if (pRemoteService == nullptr) {
      Serial.print("Failed to find our service UUID: ");
      Serial.println(serviceUUID.toString().c_str());
      return false;
    }
    Serial.println(" - Found our service");


    // Obtain a reference to the characteristic in the service of the remote BLE server.
    pRemoteCharacteristic = pRemoteService->getCharacteristic(charUUID);
    if (pRemoteCharacteristic == nullptr) {
      Serial.print("Failed to find our characteristic UUID: ");
      Serial.println(charUUID.toString().c_str());
      return false;
    }
    Serial.println(" - Found our characteristic");

//    // Read the value of the characteristic.
//    std::string value = pRemoteCharacteristic->readValue();
//    Serial.print("The characteristic value was: ");
//    Serial.println(value.c_str());

//    pRemoteCharacteristic->registerForNotify(notifyCallback);
    return true;
}
/**
 * Scan for BLE servers and find the first one that advertises the service we are looking for.
 */
class MyAdvertisedDeviceCallbacks: public BLEAdvertisedDeviceCallbacks {
 /**
   * Called for each advertising BLE server.
   */
  void onResult(BLEAdvertisedDevice advertisedDevice) {
    Serial.print("BLE Advertised Device found: ");
    Serial.println(advertisedDevice.toString().c_str());

    // We have found a device, let us now see if it contains the service we are looking for.
    if (advertisedDevice.haveServiceUUID() && advertisedDevice.getServiceUUID().equals(BLEUUID(UUID_BASE))) {

      //
      Serial.print("Found our device!  address: ");
      advertisedDevice.getScan()->stop();

      pServerAddress = new BLEAddress(advertisedDevice.getAddress());
      doConnect = true;

    } // Found our server
  } // onResult
}; // MyAdvertisedDeviceCallbacks

void BLETask(void*param){
	  Serial.println("Starting Arduino BLE Client application...");
	  if(!BLEDevice::getInitialized()){
	  BLEDevice::init("");
	  BLEScan* pBLEScan = BLEDevice::getScan();
	  pBLEScan->setAdvertisedDeviceCallbacks(new MyAdvertisedDeviceCallbacks());
	  pBLEScan->setActiveScan(true);
	  pBLEScan->start(10);
	  } else {

	  // Retrieve a Scanner and set the callback we want to use to be informed when we
	  // have detected a new device.  Specify that we want active scanning and start the
	  // scan to run for 30 seconds.
	  BLEScan* pBLEScan = BLEDevice::getScan();
	  pBLEScan->start(10);
	  }
	for(int i = 0 ; i < 5;i++){

		  // If the flag "doConnect" is true then we have scanned for and found the desired
		  // BLE Server with which we wish to connect.  Now we connect to it.  Once we are
		  // connected we set the connected flag to be true.
		  if (doConnect == true) {
		    if (connectToServer(*pServerAddress)) {
		      Serial.println("We are now connected to the BLE Server.");
		      connected = true;
		    } else {
		      Serial.println("We have failed to connect to the server; there is nothin more we will do.");
		    }
		    doConnect = false;
		  }

		  // If we are connected to a peer BLE Server, update the characteristic each time we are reached
		  // with the current time since boot.
		  if (connected) {
		    String newValue = "Connected";
		    Serial.println("Connected ");
		    // Set the characteristic's value to be the array of bytes that is actually a string.
		    pRemoteCharacteristic->writeValue(newValue.c_str(), newValue.length());
		    i=5;//break
		    for(int l = 0 ; l < 4 ; l++){
			gpio_set_level(LED_G, 0);
			gpio_set_level(LED_Y, 0);
			vTaskDelay(200 / portTICK_PERIOD_MS);
			gpio_set_level(LED_G,1);
			gpio_set_level(LED_Y,1);
			vTaskDelay(200 / portTICK_PERIOD_MS);
			gpio_set_level(LED_G, 0);
			gpio_set_level(LED_Y, 0);
		   }
		  }

//		  delay(1000); // Delay a second between loops.
	}
}
static void mpu_task(void* param) {
	mpu6050L.init(I2C_ADDRESS_L,GPIO_NUM_21, GPIO_NUM_22,I2C_SPEED,I2C_NUM_0);
	//SDA SCL

	mpu6050L.calcGyroOffsets(true);
	Kalman myFilterGX(0.125, 32, 1023, 0);
	Kalman myFilterGY(0.125, 32, 1023, 0);
	Kalman myFilterGZ(0.125, 32, 1023, 0);
	filter_esp.begin(1000000.0/microsPerReading);
    String newValue = "1IMUOK ";
    if(connected){
    pRemoteCharacteristic->writeValue(newValue.c_str(), newValue.length());
    }
	Serial.printf("%s \r\n",newValue.c_str());// realtime

#ifdef secondIMU
	mpu6050H.init(I2C_ADDRESS_H,GPIO_NUM_21, GPIO_NUM_22,I2C_SPEED,I2C_NUM_0);
	mpu6050H.calcGyroOffsets(true);
	Kalman myFilterGXH(0.125, 32, 1023, 0);
	Kalman myFilterGYH(0.125, 32, 1023, 0);
	Kalman myFilterGZH(0.125, 32, 1023, 0);
	filter_espH.begin(1000000.0/microsPerReading);
    newValue = "2IMUOK ";
    if(connected){
    pRemoteCharacteristic->writeValue(newValue.c_str(), newValue.length());
    }
	Serial.printf("%s \r\n",newValue.c_str());// realtime

#endif
#ifdef threeIMU
	Madgwick filter_espL_2;
    pinMode(GPIO_ON_IMU_3, OUTPUT);
    digitalWrite(GPIO_ON_IMU_3, LOW);   // turn the LED on (HIGH is the voltage level)
    delay(1000);
	mpu6050L_2.init(I2C_ADDRESS_L,IMU_THREE_SDA, IMU_THREE_SCL,I2C_SPEED,I2C_NUM_1);
	mpu6050L_2.calcGyroOffsets(true);
	filter_espL_2.begin(1000000.0/microsPerReading);

#endif
	/*
	 * button
	 */
	gpio_pad_select_gpio(BUTTON);
	gpio_set_direction(BUTTON, GPIO_MODE_INPUT);
	gpio_set_pull_mode(BUTTON, GPIO_PULLUP_ONLY);
#ifdef DRAW
	gpio_pad_select_gpio(BUTTON_MODE);
	gpio_set_direction(BUTTON_MODE, GPIO_MODE_INPUT);
	gpio_set_pull_mode(BUTTON_MODE, GPIO_PULLUP_ONLY);
#endif
	bool pressed = false;
	bool showResust = false;

    for(int i = 0 ; i < 5 ; i++){
	gpio_set_level(LED_Y, 0);
	vTaskDelay(200 / portTICK_PERIOD_MS);
	gpio_set_level(LED_Y,1);
	vTaskDelay(200 / portTICK_PERIOD_MS);
	gpio_set_level(LED_Y, 0);
   }
    newValue = "3IMUOK ";
    if(connected){
    pRemoteCharacteristic->writeValue(newValue.c_str(), newValue.length());
    }
	Serial.printf("%s \r\n",newValue.c_str());// realtime

	microsPrevious = micros();

	while(1){
		unsigned long microsNow;
//		if (digitalRead(BUTTON) == LOW) {
			if (!pressed) {
				/*
				 * use send to robot
				 * only send when recive done data
				 * if no press and have data so send signal
				 */
					if(haveData){
					xEventGroupSetBits(imu_event_group, DONE_RICIVE_DATA_BIT);
					}
//				x_0 = 0.0;
//				y_0 = 0.0;
//				filter_esp.clean();  unblock if refest but must bot adn clen
#ifdef secondIMU

//				filter_espH.clean();
#endif
				pressed = true; //khi ko nhan press false
//				haveData =true;
				count = 0; //when not press cout = 0
			}

			microsNow = micros();
//			deltaTime =microsNow - microsPrevious;
			if (microsNow - microsPrevious >= microsPerReading) {
//				Serial.printf("deltaTime: %lu\r\n",deltaTime);
//				Serial.printf("count: %d\r\n",count);
				/*****************************************************************/
				deltaTime =microsNow - microsPrevious;


				mpu6050L.readGyro();
				float gyroX = mpu6050L.getGyroX();
				float gyroY = mpu6050L.getGyroY();
				float gyroZ = mpu6050L.getGyroZ();
				mpu6050L.readAccel();
				float accelX = mpu6050L.getAccX();
				float accelY = mpu6050L.getAccY();
				float accelZ = mpu6050L.getAccZ();
				gtx = gyroX;
				gty = gyroY;
				gtz = gyroZ;        //deg/s
				atx = accelX;
				aty = accelY;        //g
				atz = accelZ;
//				gtx = gtx * gyroScale;
//				gty = gty * gyroScale;
//				gtz = gtz * gyroScale;
				filter_esp.updateIMU(gtx, gty, gtz, atx, aty, atz);
//				pitch =(float) myFilterGX.getFilteredValue((double)(filter_esp.getPitch()));
//				roll = (float) myFilterGY.getFilteredValue((double)(filter_esp.getRoll()));
//				yaw = (float) myFilterGZ.getFilteredValue((double)(filter_esp.getYaw()));

				if (!MODE_CAR_AND_DIGI) { //modehand draw
					pitch = filter_esp.getPitch();
					roll = filter_esp.getRoll();
					yaw = filter_esp.getYaw();
				} else {
					pitch =filter_esp.getPitchRadians();
					roll = filter_esp.getRollRadians();
					yaw = filter_esp.getYawRadians();
				}
//				pitch = filter_esp.getPitch();
//				roll = filter_esp.getRoll();
//				yaw = filter_esp.getYaw();
				/*
				 * calculator distance
				 */


#ifdef secondIMU
				/*******************************************************************/
				mpu6050H.readAccel();
				mpu6050H.readGyro();
				float gyroXH = mpu6050H.getGyroX();
				float gyroYH = mpu6050H.getGyroY();
				float gyroZH = mpu6050H.getGyroZ();
				float accelXH = mpu6050H.getAccX();
				float accelYH = mpu6050H.getAccY();
				float accelZH = mpu6050H.getAccZ();
				gtxH = gyroXH;
				gtyH = gyroYH;
				gtzH = gyroZH;        //deg/s
				atxH = accelXH;
				atyH = accelYH;        //g
				atzH = accelZH;
				//				gtx = gtx * gyroScale;
//				gty = gty * gyroScale;
//				gtz = gtz * gyroScale;
				filter_espH.updateIMU(gtxH, gtyH, gtzH, atxH, atyH, atzH);
//				pitchH =(float) myFilterGXH.getFilteredValue((double)(filter_espH.getPitch()));
//				rollH = (float) myFilterGYH.getFilteredValue((double)(filter_espH.getRoll()));
//				yawH = (float) myFilterGZH.getFilteredValue((double)(filter_espH.getYaw()));


				if (!MODE_CAR_AND_DIGI) { //modehand draw
					pitchH =filter_espH.getPitch();
					rollH = filter_espH.getRoll();
					yawH = filter_espH.getYaw();
				} else {
					pitchH =filter_espH.getPitchRadians();
					rollH = filter_espH.getRollRadians();
					yawH = filter_espH.getYawRadians();
					}

				/**************************************************************/
#endif
#ifdef threeIMU
				/*******************************************************************/
				mpu6050L_2.readAccel();
				mpu6050L_2.readGyro();
				float gyroXL_2 = mpu6050L_2.getGyroX();
				float gyroYL_2 = mpu6050L_2.getGyroY();
				float gyroZL_2 = mpu6050L_2.getGyroZ();
				float accelXL_2 = mpu6050L_2.getAccX();
				float accelYL_2 = mpu6050L_2.getAccY();
				float accelZL_2 = mpu6050L_2.getAccZ();
				gtxL_2 = gyroXL_2;
				gtyL_2 = gyroYL_2;
				gtzL_2 = gyroZL_2;        //deg/s
				atxL_2 = accelXL_2;
				atyL_2 = accelYL_2;        //g
				atzL_2 = accelZL_2;
				filter_espL_2.updateIMU(gtxL_2, gtyL_2, gtzL_2, atxL_2, atyL_2, atzL_2);


				if (!MODE_CAR_AND_DIGI) { //modehand draw
					pitchL_2 =filter_espL_2.getPitch();
					rollL_2 = filter_espL_2.getRoll();
					yawL_2 = filter_espL_2.getYaw();
				} else {
					pitchL_2 =filter_espL_2.getPitchRadians();
					rollL_2 = filter_espL_2.getRollRadians();
					yawL_2 = filter_espL_2.getYawRadians();
				}
				/**************************************************************/
#endif
#ifdef DRAW
#endif
				/*
				 end for realtime
				 */
				microsPrevious = microsPrevious + microsPerReading;
//				if(count%5==0)xEventGroupSetBits(imu_event_group, RICIVE_DATA_BIT);

			}

			if (digitalRead(BUTTON) == LOW) {
xEventGroupSetBits(imu_event_group, PRESS_BIT);
#ifdef secondIMU
xEventGroupSetBits(imu_event_group, RICIVE_DATA_BIT);
haveData = true;
#endif
#ifdef DRAW
if (MODE_CAR_AND_DIGI) {

//				Serial.printf("press: %d\r\n",digitalRead(BUTTON));
				//tac dung khi ve dian moi se doi toa do ve goc toa do
				if(count==0){
					x_0=-Radius*yawL_2; //in cm
					y_0=Radius*rollL_2;
				}
				count++;
		if(((count%4)==0)){// &&(count < 512)){ //tang toc
			x=-Radius*yawL_2; //in cm
			y=Radius*rollL_2;
			z=Radius*pitchL_2;
			float xx0 = x-x_0;
			float yy0 = y-y_0;

			Distance = std::sqrt(xx0*xx0+yy0*yy0); // in cm
			//gui 1 point wwhen Distance >10cm
			if(Distance > 30.0 ){
			TotalDistance +=Distance;
			if(abs(yy0)<0.00001){
				if(xx0<0) { Alpha = 90.0;} else {
					Alpha = -90.0;
				}
			} else {
			Alpha = 57.29578*atan(xx0/yy0); //so voi org
			}
			if(yy0<0) Alpha=Alpha-180.0;
			_x0 = x_0;
			_y0= y_0 ;
			x_0 = x;
			y_0 = y;
			//temp distan algel
			//de khong bi lost data

			rDistance 	= 	Distance;
			rAlpha		= 	Alpha -			tAlpha; // goc bay gio - goc truoc do (he toa do goc) = goc can quay
//			Serial.printf(">>>Alpha:%0.1f - tAlpha:%0.1f = rAlpha:%0.1f\r\n",Alpha,tAlpha, rAlpha);
			if(abs(rAlpha) < 4) rAlpha= 0;
			tAlpha = Alpha;//luu anpha truoc do lai

			xEventGroupSetBits(imu_event_group, RICIVE_POINT_DATA_BIT);
			}
			//plot when recive data
//			xEventGroupSetBits(imu_event_group, RICIVE_DATA_BIT);
			//viec nhan khien ta co data
		} else {
			 //khong chia het cho 5 thi thoi

		}
			}
#endif
			}		else {
			pressed = false; //unblock if reset cooord x=0;y=0 // sau nay se chueyn mode duoc
//			vTaskDelay(10/ portTICK_PERIOD_MS);

		}
	}
}
static void showTask(void* param) {
int Creconnect = 0;

	while(1){
		if (connected) {
			if (digitalRead(BUTTON_MODE) == LOW) {
				Creconnect++;
//			Serial.printf("%d\r\n",Creconnect);
				if (Creconnect > 333) {
					Creconnect = 0;
					MODE_CAR_AND_DIGI=(!MODE_CAR_AND_DIGI);
					String _Str =
							(MODE_CAR_AND_DIGI==true)?
									String("MOD:CAR_DIGI") :
									String("MOD:HAND");
					if (connected) {
						pRemoteCharacteristic->writeValue(_Str.c_str(),	_Str.length());
					}
					Serial.printf("%s\r\n",_Str.c_str());
				}

			}
		}
//		unsigned long microsNow_2;
//		microsNow_2 = micros();
//			deltaTime =microsNow - microsPrevious;
//		if (microsNow_2 - microsPrevious_2 >= microsPerReading) {
//			deltaTime_2 =microsNow_2 - microsPrevious_2;
#ifdef threeIMU_
				/*******************************************************************/
				mpu6050L_2.readAccel();
				mpu6050L_2.readGyro();
				float gyroXL_2 = mpu6050L_2.getGyroX();
				float gyroYL_2 = mpu6050L_2.getGyroY();
				float gyroZL_2 = mpu6050L_2.getGyroZ();
				float accelXL_2 = mpu6050L_2.getAccX();
				float accelYL_2 = mpu6050L_2.getAccY();
				float accelZL_2 = mpu6050L_2.getAccZ();
				gtxL_2 = gyroXL_2;
				gtyL_2 = gyroYL_2;
				gtzL_2 = gyroZL_2;        //deg/s
				atxL_2 = accelXL_2;
				atyL_2 = accelYL_2;        //g
				atzL_2 = accelZL_2;
				filter_espL_2.updateIMU(gtxL_2, gtyL_2, gtzL_2, atxL_2, atyL_2, atzL_2);
				pitchL_2 =filter_espL_2.getPitch();
				rollL_2 = filter_espL_2.getRoll();
				yawL_2 = filter_espL_2.getYaw();

				/**************************************************************/
				microsPrevious_2 = microsPrevious_2 + microsPerReading;

#endif
//		}
		/*
		 * press va reaad all data pass
		 * press: clear adn read again
		 */
	EventBits_t uxBits;
	uxBits = xEventGroupWaitBits(imu_event_group,
			DONE_RICIVE_DATA_BIT | RICIVE_DATA_BIT | PRESS_BIT|RICIVE_POINT_DATA_BIT , false, false, (TickType_t) 0);
	if((uxBits & ( PRESS_BIT|DONE_RICIVE_DATA_BIT))
					== (PRESS_BIT |DONE_RICIVE_DATA_BIT)) {
			xEventGroupClearBits(imu_event_group,PRESS_BIT);
			xEventGroupClearBits(imu_event_group,DONE_RICIVE_DATA_BIT);
			xEventGroupClearBits(imu_event_group,RICIVE_DATA_BIT);

			haveData =  false;
			String mStr = "100;";
			if (MODE_CAR_AND_DIGI) {
				if ((cPoint < 10) && (cPoint >= 1)) {
					for (int i = 0; i < cPoint; i++) {
						mStr += (vAlpha[i] + vDistance[i]);
					}
					mStr += "1000;";
					if (connected) {
						pRemoteCharacteristic->writeValue(mStr.c_str(),mStr.length());
						Serial.printf(">>>Send: %s\r\n", mStr.c_str());
					}
					vDistance.clear();
					vAlpha.clear();
				}
			}
			mStr ="Clean \r\n";
			if(connected){
				pRemoteCharacteristic->writeValue(mStr.c_str(), mStr.length());
			}
			cPoint = 0;
			Serial.printf("Clean \r\n");
		}
 else
	if ((uxBits & ( RICIVE_DATA_BIT))
				== ( RICIVE_DATA_BIT)) {
		xEventGroupClearBits(imu_event_group,RICIVE_DATA_BIT);
//		Serial.printf("Qa: %0.4f %0.4f %0.4f %lu %d\r\n",yaw,pitch,roll,deltaTime,count);// realtime // use plot fron serial
//		Serial.printf("Alpha: %0.1f Distance:%0.1f \r\n",Alpha,Distance);//for gobot
//		Serial.printf("Or: %0.4f %0.4f %0.4f %0.4f %0.4f %0.4f %lu\r\n",yawH,pitchH,rollH,yaw,pitch,roll,deltaTime);// realtime
			if (!MODE_CAR_AND_DIGI) {
				String mStr = "Or:" + String(yawH, 0) + ":" + String(pitchH, 0)+ ":" + String(rollH, 0);
#ifdef secondIMU
				mStr = mStr + ":" + String(yaw, 0) + ":" + String(pitch, 0)+ ":" + String(roll, 0);
#endif
#ifdef threeIMU
				mStr = mStr + ":" + String(yawL_2, 0) + ":"	+ String(pitchL_2, 0) + ":" + String(rollL_2, 0);
#endif
//				Serial.printf("%lu\r\n", deltaTime);
				Serial.printf("%s\r\n", mStr.c_str());
				if (connected) {
					pRemoteCharacteristic->writeValue(mStr.c_str(),
							mStr.length());
				}
			}  else{
#ifdef DRAW


		/*
		 * use send plot in laptop
		 */
//		Serial.printf("X:%0.1f Y:%0.1f %d \r\n",x,y,count);
//		String mStr ="Qa: "+String(yaw,3) +  " " + String(pitch,3) + " "+ String(roll,3)+" "+String(Alpha);

		String mStr ="Co:"+String(x,0)+":"+String(y,0);
		if(connected){
		pRemoteCharacteristic->writeValue(mStr.c_str(), mStr.length());
		}
		Serial.printf("Co:%0.1f:%0.1f\r\n",x,y);
//		Serial.printf("%lu\r\n",deltaTime);
		if((uxBits & ( RICIVE_POINT_DATA_BIT))
					== ( RICIVE_POINT_DATA_BIT)){
		xEventGroupClearBits(imu_event_group,RICIVE_POINT_DATA_BIT);
				/*
				 * when recive a new data calculator distace and alpha  , append  it
				 */
				String mStr ="Po:"+String(_x0,3)+":"+String(_y0,3) +":"+String(x_0,3)+":"+String(y_0,3)+":" +String(rDistance)+":" +String(Alpha)+":" +String(rAlpha);
				if(connected){
				pRemoteCharacteristic->writeValue(mStr.c_str(), mStr.length());
				}
//				Serial.printf("Alphat:%0.1f - tAlphat:%0.1f = rAlphat:%0.1f\r\n",Alphat,tAlphat, rAlphat);
				tAlphat = Alphat;
//				Serial.printf("%d |%0.1f %0.1f |%0.1f %0.1f| %0.1f||Alpha-Alphat: %0.1f %0.1f||rAlpha-rAlphat: %0.1f %0.1f\r\n=======\r\n",cPoint,x_0,y_0,_x0,_y0,rDistance,Alpha,Alphat,rAlpha,rAlphat);
				Serial.printf("Po:%0.1f:%0.1f:%0.1f:%0.1f:%0.1f:%0.1f:%0.1f\r\n",_x0,_y0,x_0,y_0,rDistance,Alpha,rAlpha);
				if(cPoint < 10 ){ //only draw N point
	//			String mStr = "Di: "+ String(rDistance,3)+" "+String(rAlpha,1);
	//			Serial.printf("____rAlpha: %0.1f   ____rDistance:%0.1f \r\n",rAlpha,rDistance);
				String sDistance = "200,"+String(rDistance,0)+",-1;";
				String sAlpha = "210,"+String(rAlpha,0)+";";
				rDistance = 0.0;
				vDistance.push_back(sDistance);
				vAlpha.push_back(sAlpha);
				}
				cPoint++;

			}

#endif
			}
	}
	else {
		vTaskDelay(10/portTICK_PERIOD_MS);
	}

	}

}

void app_main(void)
{
	Serial.begin(115200);
    esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES) {
        // OTA app partition table has a smaller NVS partition size than the non-OTA
        // partition table. This size mismatch may cause NVS initialization to fail.
        // If this happens, we erase NVS partition and initialize NVS again.
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }
    ESP_ERROR_CHECK( err );
    led();
	BLETask(NULL);
	imu_event_group = xEventGroupCreate();
	xTaskCreatePinnedToCore(&mpu_task, "mpu_task", 20000, NULL, 2, NULL,taskCore_r);
	xTaskCreatePinnedToCore(&showTask, "show", 20000, NULL, 2, NULL,taskCore_s);
}

